const calendarContainerId = "calendarView"
const monthDisplaySpanId = "currentMonth";
const yearDisplaySpanId = "currentYear"

function getDateElement(startOfCalendar) {
    let total = 60
    let full = Math.round(Math.random() * 40)
    let panel = total - full
    let walkins = Math.round(Math.random() * 20) 
    return `
<div class="col-sm-10 border border-secondary calendar-date">
    <div class="row">
        <div class="col-sm-10 col-md-3 mt-5 mb-5">
            <h5 class="text-center"><a href="docketDetails">${startOfCalendar.format('MMM Do YYYY')}</a></h5>
        </div>
        <!-- <div class="col-sm-7 text-md-right text-sm ml-auto">
            ${startOfCalendar.format('dddd')}
        </div> -->
        <div class="col-sm-10 col-md-3 mt-5 mb-5">
            <div class="border border-primary rounded text-center p-2">
                <span class="h5 mr auto">
                    <i class="las la-user"></i>
                    <i class="las la-user"></i>
                    <i class="las la-user"></i>
                    Full
                </span>
                <span class="h5 text-danger ml-auto">${full}</span>
            </div>
        </div>
        <div class="col-sm-10 col-md-3 mt-5 mb-5">
            <div class="border border-primary rounded text-center p-2">

            <span class="h5 mr auto">
                <i class="las la-user"></i>
                <i class="las la-user"></i>
                Panel
            </span>
            <span class="h5 text-success ml-auto">${panel}</span>
            </div>
        </div>
        <div class="col-sm-10 col-md-3 mt-5 mb-5">
            <div class="border border-primary rounded text-center p-2">
                <span class="h5 mr auto">
                    <i class="las la-walking"></i>
                    Walkins
                </span>
                <span class="h5 ml-auto">${walkins}</span>
            </div>
        </div>
    </div>
    <div class="row">
        
    </div>
</div>
    `
}
function loadCalendar(currentDate) {
    
    $(`#${monthDisplaySpanId}`).html(currentDate.format('MMMM'));
    $(`#${yearDisplaySpanId}`).html(currentDate.format('YYYY'));
    const calendarContainer = $(`#${calendarContainerId}`);
    calendarContainer.html('')
    let startOfMonth = moment(currentDate).startOf('m');
    let startOfCalendar = currentDate
    for (let i = 0; i < 30; i++) {
        let k = Math.random() * 100
        if(k < 33){
            calendarContainer.append(getDateElement(startOfCalendar));    
        }
        startOfCalendar.subtract(1, 'd')
    }

    $('.calendar-date').on('click', ()=> {
        window.location = "/docketDetails"
    })
}



$(document).ready(async () => {
    $('.date').datepicker({
        autoclose: true,
        todayHighlight: true
    })
    $('#dateSearch').on('change', (event)=>{
        let element = $(event.target)
        loadCalendar(moment(element.val()));
    })
    loadCalendar(moment().add(7, 'd'))
})

