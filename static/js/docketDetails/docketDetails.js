const typeParams = [
    'full', 'panel', 'walkin'
]


const categories = [
    'Effective Interview',
    'FCOR Draft',
    'Extraordinary Interview',
    'Request for Review',
    'Void Interview'
]

function getData(group){
    let data = []
    for(let i = 0; i < 30; i++){
        let name = 'Inmate ' + i
        data.push({
            name,
            visitor_id: Math.floor((Math.random() *  10000)),
            item_id: Math.floor(Math.random() * 100), 
            category: categories[Math.floor((Math.random() * 5))],
            addendum: ((Math.random() * 100) < 25),
            group,
        })
    }
    return data
}

function getTable(group){
    let data = getData(group)
    $(`#${group}Table`).DataTable({
        data,
        responsive: true,
        columns: [
            { 
                title: "Name",
                data: 'name'
            },
            { 
                title: "Visitor ID #",
                data: "visitor_id"
            },
            { 
                title: "Item #",
                data: 'item_id'
            },
            { 
                title: "Category",
                data: 'category'
            },
            {
                title: '',
                data: ''
            }
        ],
        columnDefs: [
            {
                targets: 0,
                render: function(data, type, row, meta){
                    return `<a href="inmateDocket">${row.name}</a>`
                }
            },
            {
                targets: -1,
                render: function(data, type, row, meta){
                    if(row.addendum){
                        return '<span class="badge badge-warning">Addendum</span>'
                    } else {
                        return ''
                    }
                }
            }
        ]
    });
}
$(document).ready(()=> {
    for(const type of typeParams){
        getTable(type)
    }
})
