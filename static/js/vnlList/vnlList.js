
// program to generate random strings

// declare all characters
const characters ='ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789 ';

function generateString(length) {
    let result = ' ';
    const charactersLength = characters.length;
    for ( let i = 0; i < length; i++ ) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }

    return result;
}


function getData(){
    let data = []
    for(let i = 0; i < 30; i++){
        let name = 
        data.push({
            id: i,
            name: 'Visitor ' + i,
            email: 'someone@example.com',
            address: '1945 Southern Avenue <br /> Harvester <br /> Mo-633301',
            phone: '111-222-3333',
            type: Math.random() > 0.5 ? 'Support' : 'Victim',
            date: moment().subtract((Math.floor(Math.random() * 365)), 'd').format('MMM Do YYYY'),
            comment: generateString(100)
        })
    }
    return data
}

function getTable(){
    let data = getData()
    $(`#vnlTable`).DataTable({
        data,
        responsive: true,
        columns: [
            { 
                title: "Type",
                data: "type",
                width: 100
            },
            { 
                title: "Name",
                data: 'name',
                width: 140
            },
            {
                title: "Phone",
                data: "phone",
                width: 100
            },
            {
                title: "Email",
                data: "email",
                width: 140
            },
            {
                title: "Address",
                data: "address",
                width: 200

            },
            { 
                title: "Contact Date",
                data: 'date',
                width: 150
            },
            { 
                title: "Comment",
                data: 'comment'
            },
            {
                title: 'Actions',
                data: ''
            }
        ],
        columnDefs: [
            {
                targets: 6,
                render: function(data, type, row, meta){
                    if(row.comment){
                        if(row.comment.length > 20){
                            let display = row.comment.substring(0, 19)
                            let collapsed = row.comment.substring(20, row.comment.length - 1)
                            return `
                                <p>
                                    ${display}
                                    <span class="collapse" id="commentCollapse_${row.id}">
                                        ${collapsed}
                                    </span> 
                                    <a href="#" class="commentCollapse" data-toggle="collapse" data-target="#commentCollapse_${row.id}">Read More...</a>
                                </p>
                            `
                        }
                    }
                    return row.comment
                }
            },
            {
                targets: -1,
                render: function(data, type, row, meta){
                    return `
                    <div class="btn-group" role="group" aria-label="Basic example">
                        <a class="btn btn-outline-primary m-1 p-1" href="#">
                            View Sent Letter
                        </a>
                    </div>`
                }
            }
        ]
    });
}

$(document).ready(()=> {
    $('.date').datepicker({
        autoclose: true,
        todayHighlight: true
    })
    getTable()
    $('.commentCollapse').click((event)=> {
        let element = $(event.target)
        if(element.text() == `Read More...`){
            element.html('Less')
        } else {
            element.html(`Read More...`)
        }
    })
})