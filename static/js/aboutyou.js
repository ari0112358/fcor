
function getClientInfo(){
    // Ip address
    $.getJSON('https://json.geoiplookup.io/api?callback=?', function(data) {
        console.log(data)
        $('#ip_address').html(data.ip)
    });

    // Browser Info
    $('#browser_info').html(
        `<p><strong>Browser name:</strong> ` + navigator.appName + `</p>
         <p><strong>Engine:</strong> ` + navigator.product + `</p>
         <p><strong>Version:</strong> ` + navigator.appVersion + `</p>
         <p><strong>Language:</strong> ` + navigator.language + `</p>
         <p><strong>Online:</strong> ` + navigator.onLine + `</p>
         <p><strong>Platform:</strong> ` + navigator.platform + `</p>`
    )

    $('#screen_info').html(
        `<p><strong>Screen width:</strong> ` + screen.width + `</p>
         <p><strong>Screen height:</strong> ` + screen.height + `</p>
         <p><strong>Color depth:</strong> ` + screen.colorDepth + `</p>
         <p><strong>Pixel depth:</strong> ` + screen.pixelDepth + `</p>`
    )
}

jQuery(document).ready(function () {
    getClientInfo()
});