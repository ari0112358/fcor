const dragEvents = ['dragenter', 'dragover', 'dragleave', 'drop'];
const dragHighlight = ['dragenter', 'dragover'];
const dragUnighlight = ['dragleave', 'drop'];
const inputRange = document.querySelectorAll('.editor input');

const dropArea = document.getElementById('drop-area');
const iconElement = document.querySelector('.circle');
const editorElement = document.querySelector('.editor');

let _files = []

const preventDefaults = (e) => {
    e.preventDefault();
    e.stopPropagation();
};

const highlight = () => {
    iconElement.classList.add('highlight');
};

const unhighlight = () => {
    iconElement.classList.remove('highlight');
};

const previewFile = (file) => {
    let reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onloadend = function () {
        /* let img = document.createElement('img');
        img.src = reader.result;
        document.getElementById('gallery').appendChild(img);
        editorElement.classList.add('is-visible'); */
        $('#fileName').html(file.name)
        $('#fileType').html(file.type)
        $('#fileSize').html(`${Math.round(file.size/1024 * 100) / 100} Kb`)
        $("#status").removeClass('d-none')
    }
};

const handleFiles = (files) => {
    for(i = 0; i < files.length; i++){
        let file = files.item(i);
        _files.push(file)
        $('#uploadedFiles').append(`
            <span class="badge badge-primary m-2 selectedFile" style="font-size:smaller">
                <span> ${file.name} </span>
                 <i style="font-size:large" class="closebutton las la-times-circle pt-1"></i>
            </span>`);
    }
    $('.closebutton').off('click')
    $('.closebutton').on('click', function(event){
        let item = $(event.target).closest('.selectedFile')
        item.remove();
    })
};

const handleDrop = (e) => {
    let dt = e.dataTransfer;
    let files = dt.files;
    console.log('files', files);
    handleFiles(files);
};

function handleUpdate() {
    console.log(this.value);
    const suffix = this.dataset.unit;
    document.documentElement.style.setProperty(`--${this.name}`, this.value + suffix);
}

inputRange.forEach(input => input.addEventListener('change', handleUpdate));
inputRange.forEach(input => input.addEventListener('mousemove', handleUpdate));

dragEvents.forEach(eventName => {
    dropArea.addEventListener(eventName, preventDefaults, false)
});

/* dragHighlight.forEach(eventName => {
    dropArea.addEventListener(eventName, highlight, false)
});

dragUnighlight.forEach(eventName => {
    dropArea.addEventListener(eventName, unhighlight, false)
}); */


dropArea.addEventListener('drop', handleDrop, false);

$(document).ready(()=> {
    $('#clear').on('click', function(){
        Swal.fire({
            title: 'Warning!',
            text: 'This will remove all your selected file(s)',
            icon: 'warning',
            confirmButtonText: 'Yes',
            showCancelButton: true,
            confirmButtonColor: '#007bff',
            cancelButtonColor: '#dc3545',
        }).then((result)=> {
            if(result.isConfirmed){
                _files = []
                $('#uploadedFiles').html('')
            }
        })
    })

    $('#upload').on('click', function(){
        let swalHtml = `<ul>`
        _files.forEach((file) => {
            swalHtml += `<li>${file.name.trim()}</li>`
        })
        swalHtml += `</ul>`
        Swal.fire({
            title: 'Upload Confirmation',
            html: swalHtml,
            icon: 'info',
            confirmButtonText: 'Yes',
            showCancelButton: true,
            confirmButtonColor: '#007bff',
            cancelButtonColor: '#dc3545',
        }).then((result)=> {
            if(result.isConfirmed){
                _files = []
                $('#uploadedFiles').html('')
            }
        })
    })
})