const inmateListForm = "collapseOne"
const inmateListTable = "collapseTwo"
const searchButtonId = "inmateSearch";
let data = []
$(document).ready(async ()=> {
    
    $('.date').datepicker({
        autoclose: true,
        todayHighlight: true
    })/* .datepicker('update', new Date()) */

    //await loadPayeeTable(data)
    //$('#inmateListTable').collapse('toggle');
    try{
        $(`#${searchButtonId}`).on('click',async ()=>{
            
            await loadPayeeTable(staticData);
            let inmateListFormE = $(`#${inmateListForm}`)
            if(inmateListFormE.hasClass('show')){
                inmateListFormE.collapse('toggle')
            } 
            let inmateListTableE = $(`#${inmateListTable}`)
            if(!inmateListTableE.hasClass('show')){
                inmateListTableE.collapse('toggle');
            }
            inmateListTableE.removeClass('d-none');
        })
    } catch(error){
        console.log(error)
    }
});

const inmateTableId = "inmateTable";
let inmateTable = null
async function loadPayeeTable(data){
    if(inmateTable != null) await inmateTable.destroy();
    inmateTable = $(`#${inmateTableId}`).DataTable({
        data,
        responsive: true,
        columns: [
            { 
                title: "Name",
                data: 'name'
            },
            { 
                title: "DC #",
                data: "dcNumber"
            },
            { 
                title: "Agenda Date",
                data: 'agendaDate'
            },
            { 
                title: "LID",
                data: 'lid'
            },
            { 
                title: "Issue",
                data: 'issue'
            },
            { 
                title: "Panel",
                data: 'panel'
            },
            { 
                title: "Creator/Modifier",
                data: 'creator'
            },
            { 
                title: "Create Date",
                data: 'createDate'
            },
            { 
                title: "Status",
                data: 'status'
            },
            { 
                title: "Page",
                data: 'page'
            },
            { 
                title: "Case Types",
                data: 'caseTypes'
            },
            {
                title: "Actions",
                data: ''
            }
        ],
        columnDefs: [
            {
                targets: 2,
                render: function(data, type, row, meta){
                    return (new Date(row.agendaDate)).toLocaleDateString("en-US")
                }

            },
            {
                targets: 7,
                render: function(data, type, row, meta){
                    return (new Date(row.createDate)).toLocaleDateString("en-US")
                }

            },
            {
                targets: 8,
                render: function(data, type, row, meta){
                    let badgeText = ''
                    if(data == 1){
                        badgeText ='<span class="badge badge-success">Active</span>'
                    } else {
                        badgeText ='<span class="badge badge-danger">Inactive</span>'
                    } 
                    return badgeText;
                }
            },
            {
                targets: -1,
                render: function(data, type, row, meta){
                    return `
                    <div class="btn-group" role="group" aria-label="Basic example">
                        <a class="btn btn-outline-primary m-1 p-1" href="/visitorList?dc=${row.dcNumber}">
                            <i class="las la-user-friends"style="font-size: 1.2rem;"></i>
                        </a>
                        <a class="btn btn-outline-primary m-1 p-1" href="/visitorList?dc=${row.dcNumber}">
                            <i class="las la-edit" style="font-size: 1.2rem;"></i>
                        </a>
                        <a class="btn btn-outline-primary m-1 p-1" href="/visitorList?dc=${row.dcNumber}">
                        <i class="las la-minus-circle" style="font-size: 1.1rem; color: red"></i>
                        </a>
                        <a class="btn btn-outline-primary m-1 p-1" href="/vnlList?dc=${row.dcNumber}">
                            <i class="las la-mail-bulk"></i>
                        </a>
                        
                    </div>`
                }
            },
            

        ],
    });
    $(`#${inmateTableId}`).css('width', '100%');
}