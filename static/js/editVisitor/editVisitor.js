// program to generate random strings

// declare all characters
const characters ='ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789 ';

function generateString(length) {
    let result = ' ';
    const charactersLength = characters.length;
    for ( let i = 0; i < length; i++ ) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }

    return result;
}

const visitorTableId = "commentsTable";
let visitorTable = null
async function loadVisitorTable(data) {
    if (visitorTable != null) await visitorTable.destroy();
    visitorTable = $(`#${visitorTableId}`).DataTable({
        data,
        responsive: true,
        columns: [
            {
                title: "Comment",
                data: ''
            },
            {
                title: "User",
                data: 'user'
            },
            {
                title: "Date",
                data: 'date'
            }
        ],
        columnDefs: [
            {
                targets: 0,
                render: function(data, type, row, meta){
                    /* if(row.comment){
                        if(row.comment.length > 20){
                            let display = row.comment.substring(0, 19)
                            let collapsed = row.comment.substring(20, row.comment.length - 1)
                            return `
                                <p>
                                    ${display}
                                    <span class="collapse" id="commentCollapse_${row.id}">
                                        ${collapsed}
                                    </span> 
                                    <a href="#" class="commentCollapse" data-toggle="collapse" data-target="#commentCollapse_${row.id}">Read More...</a>
                                </p>
                            `
                        }
                    } */
                    return row.comment
                }
            },
            {
                targets: 2,
                render: function(data, type, row, meta){
                    return moment(row.date).format('MMM Do YYYY')
                }

            }
        ]
    });
}

$(() => {
    $('#firstName').val('John')
    $('#lastName').val('Doe')
    let numComments = 100
    let comments = []
    for (let i = 0; i < numComments; i++) {
        let commentTime = moment()
        let commentDate = moment().subtract(Math.round(Math.random() * 500), 'd')
        comments[i] = {
            comment: generateString(100),
            user: `User ${Math.round(Math.random() * 200)}`,
            date: commentDate
        }
    }
    comments = comments.sort(function (c1, c2) {
        if (moment(c1.date) > moment(c2.date)) {
            return 1
        } else {
            return -1
        }
    })
    loadVisitorTable(comments)
})