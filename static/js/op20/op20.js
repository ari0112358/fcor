let staticData = []
function generateRandomData(){
    for( let i = 0; i < 1000; i++){
        staticData.push({
            name: (Math.random() + 1).toString(36).substring(7),
            id: Math.floor(Math.random() * 20000),
            num_offenders: Math.floor(Math.random() * 10),
            check_amount: (Math.random() * 100).toFixed(2),
            check_end_number: Math.floor(Math.random() * 20000),
            check_status: Math.floor(Math.random() * 2),
        })
    }
}

const searchButtonId = "payeeSearch";
$(document).ready( function() {
    generateRandomData()
    try{
        $(`#${searchButtonId}`).on('click',async ()=>{
            await loadPayeeTable(staticData);
        })
    } catch(error){
        console.log(error)
    }
});

const payeeTableId = "payeeTable";
const payeeModalId = "payeeModal";
const editButtonId = "editPayee";
const saveButtonId = "savePayee";
let payeeTable = null;
async function loadPayeeTable(data){
    if(payeeTable != null) await payeeTable.destroy();
    payeeTable = $(`#${payeeTableId}`).DataTable({
        data: staticData,
        columns: [
            { 
                title: "Name",
                data: 'name'
            },
            { 
                title: "Payee Id",
                data: "id"
            },
            { 
                title: "Num Offenders",
                data: 'num_offenders'
            },
            { 
                title: "Check Amount",
                data: 'check_amount'
            },
            { 
                title: "Check End Number",
                data: 'check_end_number'
            },
            { 
                title: "Check Status",
                data: 'check_status'
            }
        ],
        columnDefs: [
            /* {
                targets: 0,
                render: function(data, type, row, meta){
                    return Math.floor(Math.random() * 20000);
                }
            },
            {
                targets: 1,
                render: function(data, type, row, meta){
                    return `<span class="clickable"><u>${row.name}</u></span>`
                }
            },
            {
                targets: 6,
                render: function(data, type, row, meta){
                    return Math.floor(Math.random() * 11)
                }
            } */
        ],
        drawCallback: function(settings){
        }
    });
    
    $(`#${payeeTableId}`).removeClass('d-none');
    $(`#${payeeTableId} tbody`).on('click', 'tr', function(){
        let entry = payeeTable.row(this).data();
        console.log(entry);
        
        //Need better way maybe
        $('#payeeId').val(entry.id);
        $('#payeeId_span').html(entry.id);

        $('#payeeName').val(entry.name);
        $('#payeeName_span').html(entry.name);

        $('#num_offenders').val(entry.num_offenders);
        $('#num_offenders_span').html(entry.num_offenders);

        $('#check_amount').val(entry.check_amount);
        $('#check_amount_span').html(entry.check_amount);

        $('#check_end_number').val(entry.check_end_number);
        $('#check_end_number_span').html(entry.check_end_number);

        $('#check_status').val(entry.check_status);
        $('#check_status_span').html(entry.check_status);
        $(`#${payeeModalId}`).modal('toggle');
    })

    $(`#${editButtonId}`).on('click', function(event){
        $('.payeeDisplay').addClass('d-none');
        $('.payeeFormInput').removeClass('d-none');
        $(`#${editButtonId}`).addClass('d-none');
        $(`#${saveButtonId}`).removeClass('d-none');
    });

    $(`#${saveButtonId}`).on('click', function(event){
        $('.payeeDisplay').removeClass('d-none');
        $('.payeeFormInput').addClass('d-none');
        $(`#${editButtonId}`).removeClass('d-none');
        $(`#${saveButtonId}`).addClass('d-none');
    });
}