// program to generate random strings

// declare all characters
const characters ='ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789 ';

function generateString(length) {
    let result = ' ';
    const charactersLength = characters.length;
    for ( let i = 0; i < length; i++ ) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }

    return result;
}

function getData(){
    let data = []
    for(let i = 0; i < 30; i++){
        let name = 
        data.push({
            id: i,
            name: 'Service history ' + i,
            type: 'Service type ' + Math.floor(Math.random() * 20),
            date: moment().subtract((Math.floor(Math.random() * 365)), 'd').format('MMM Do YYYY'),
            comment: generateString(50)
        })
    }
    return data
}

function getTable(){
    let data = getData()
    $(`#serviceTable`).DataTable({
        data,
        responsive: true,
        columns: [
            { 
                title: "Name",
                data: 'name'
            },
            { 
                title: "Type",
                data: "type"
            },
            { 
                title: "Comment",
                data: 'comment'
            },
            { 
                title: "Date",
                data: 'date'
            },
        ],
        columnDefs: [
            {
                targets: 2,
                render: function(data, type, row, meta){
                    if(row.comment){
                        if(row.comment.length > 20){
                            let display = row.comment.substring(0, 19)
                            let collapsed = row.comment.substring(20, row.comment.length - 1)
                            return `
                                <p>
                                    ${display}
                                    <span class="collapse" id="commentCollapse_${row.id}">
                                        ${collapsed}
                                    </span> 
                                    <a href="#" class="commentCollapse" data-toggle="collapse" data-target="#commentCollapse_${row.id}">Read More...</a>
                                </p>
                            `
                        }
                    }
                    return row.comment
                }
            },
        ]
    });
}
$(document).ready(()=> {
    $('.date').datepicker({
        autoclose: true,
        todayHighlight: true
    })
    getTable()
    $('.commentCollapse').click((event)=> {
        let element = $(event.target)
        if(element.text() == `Read More...`){
            element.html('Less')
        } else {
            element.html(`Read More...`)
        }
    })
})