const searchButtonId = "searchVisitor";
const addVisitorForm = "collapseThree"
$(document).ready(async () => {
    /*     $('.date').datepicker({
            autoclose: true,
            todayHighlight: true
        })/* .datepicker('update', new Date()) */

    try {
        await loadVisitorTable(visitors);
    } catch (error) {
        console.log(error);
    }

    $(`#${addVisitorForm}`).collapse('toggle');
    $(`#${searchButtonId}`).on('click', async () => {
        try {
            await loadVisitorTable(visitors);
        } catch (error) {
            console.log(error);
        }
    })

});
const visitorTableId = "visitorTable";
let visitorTable = null
async function loadVisitorTable(data) {
    if (visitorTable != null) await visitorTable.destroy();
    visitorTable = $(`#${visitorTableId}`).DataTable({
        data,
        responsive: true,
        columns: [
            {
                title: "Name",
                data: ''
            },
            {
                title: "Contact Date",
                data: 'contact_date'
            },
            {
                title: "Relationship",
                data: 'relation'
            },
            {
                title: "Phone",
                data: 'home_phone'
            },
            {
                title: "Email",
                data: 'email'
            },
            {
                title: "Status",
                data: 'status'
            },
            {
                title: "Actions",
                data: ''
            }
        ],
        columnDefs: [
            {
                targets: 0,
                render: function (data, type, row, meta) {
                    let href = `/editVisitor?id=${row.email.trim()}`
                    return `<a href="${href.replace(' ', '')}">${row.first_name} ${row.last_name}</a>`
                }

            },
            {
                targets: 5,
                render: function (data, type, row, meta) {
                    let badgeText = ''
                    if (data == 1) {
                        badgeText = '<span class="badge badge-success">Active</span>'
                    } else {
                        badgeText = '<span class="badge badge-danger">Inactive</span>'
                    }
                    return badgeText;
                }
            },
            /* {
                targets: 6,
                render: function (data, type, row, meta) {
                    let numComments = 5
                    let comments = []
                    for(let i = 0; i < numComments; i++){
                        let commentTime = moment()
                        let commentDate = moment().subtract(Math.round(Math.random() * 500), 'd')
                        comments[i] = {
                            comment: `Comment ${i}`,
                            user: `User ${Math.round(Math.random() * 100)}`,
                            date: commentDate.fromNow()
                        }
                    }
                    comments = comments.sort(function(c1, c2){
                        if (c1.date > c2.date){
                            return 1
                        } else {
                            return -1
                        }
                    })
                    let commentTable = `
                        <table>
                            <thead>
                                <th>Comment</th>
                                <th>User</th>
                                <th>Time</th>
                            </thead>
                            <tbody>
                    `
                    for(const comment of comments){
                        commentTable += `
                                <tr>
                                    <td>${comment.comment}</td>
                                    <td>${comment.user}</td>
                                    <td>${comment.date}</td>
                                </tr>
                        `
                    }
                    commentTable += `
                            </tbody>
                        </table>
                    `
                    return commentTable

                }
            }, */
            {
                targets: -1,
                render: function(data, type, row, meta){
                    return `
                    <div class="btn-group" role="group" aria-label="Basic example">
                        <a class="btn btn-outline-primary m-1 p-1" href="/services?inmate=${row.email}">
                            Services
                        </a>
                        <a class="btn btn-outline-primary m-1 p-1" href="#">
                            <i class="las la-edit" style="font-size: 1.2rem;"></i>
                        </a>
                        <a class="btn btn-outline-primary m-1 p-1" href="#">
                        <i class="las la-minus-circle" style="font-size: 1.1rem; color: red"></i>
                        </a>
                        
                        
                    </div>`
                }
            }
        ],
    });
}