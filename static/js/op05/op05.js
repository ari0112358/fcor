function generateRandomData(){
    for( let i = 0; i < staticData.length; i++){
    }
}

const searchButtonId = "payeeSearch";
$(document).ready( function() {
    generateRandomData()
    try{
        $(`#${searchButtonId}`).on('click',async ()=>{
            await loadPayeeTable(staticData);
        })
    } catch(error){
        console.log(error)
    }
});

const payeeTableId = "receiptsTable";
const payeeModalId = "payeeModal";
const editButtonId = "editPayee";
const saveButtonId = "savePayee";
let payeeTable = null;
async function loadPayeeTable(data){
    if(payeeTable != null) await payeeTable.destroy();
    payeeTable = $(`#${payeeTableId}`).DataTable({
        data,
        columns: [
            { 
                title: "Receipt Date",
                data: 'receipt_date'
            },
            { 
                title: "BT ST",
                data: "bt_st"
            },
            { 
                title: "As Of Date",
                data: 'as_of_date'
            },
            { 
                title: "Offender Receipt No.",
                data: 'offender_receipt'
            },
            { 
                title: "Offender Payment",
                data: 'offender_payment'
            },
            { 
                title: "Payee Id",
                data: 'payee_id'
            },
            { 
                title: "Pfx Sequence",
                data: 'pfx_seq'
            },
            { 
                title: "Amount Disbursed",
                data: 'amount_disbursed'
            }
        ],
        columnDefs: [
            /* {
                targets: 0,
                render: function(data, type, row, meta){
                    return Math.floor(Math.random() * 20000);
                }
            },
            {
                targets: 1,
                render: function(data, type, row, meta){
                    return `<span class="clickable"><u>${row.name}</u></span>`
                }
            },
            {
                targets: 6,
                render: function(data, type, row, meta){
                    return Math.floor(Math.random() * 11)
                }
            } */
        ],
        drawCallback: function(settings){
        }
    });
    
    $(`#${payeeTableId}`).removeClass('d-none');
    $(`#${payeeTableId} tbody`).on('click', 'tr', function(){
        let entry = payeeTable.row(this).data();
        console.log(entry);
        
        //Need better way maybe
        $('#receipt_date').val(entry.receipt_date);
        $('#receipt_date_span').html(entry.receipt_date);

        $('#bt_st').val(entry.bt_st);
        $('#bt_st_span').html(entry.bt_st);

        $('#as_of_date').val(entry.as_of_date);
        $('#as_of_date_span').html(entry.as_of_date);

        $('#offender_receipt').val(entry.num_offenders);
        $('#offender_receipt_span').html(entry.num_offenders);

        $('#offender_payment').val(entry.num_offenders);
        $('#offender_payment_span').html(entry.num_offenders);

        $('#payee_id').val(entry.payee_id);
        $('#payee_id_span').html(entry.payee_id);

        $('#pfx_seq').val(entry.pfx_seq);
        $('#pfx_seq_span').html(entry.pfx_seq);

        $('#amount_disbursed').val(entry.amount_disbursed);
        $('#amount_disbursed_span').html(entry.amount_disbursed);

        $(`#${payeeModalId}`).modal('toggle');
    })

    $(`#${editButtonId}`).on('click', function(event){
        $('.payeeDisplay').addClass('d-none');
        $('.payeeFormInput').removeClass('d-none');
        $(`#${editButtonId}`).addClass('d-none');
        $(`#${saveButtonId}`).removeClass('d-none');
    });

    $(`#${saveButtonId}`).on('click', function(event){
        $('.payeeDisplay').removeClass('d-none');
        $('.payeeFormInput').addClass('d-none');
        $(`#${editButtonId}`).removeClass('d-none');
        $(`#${saveButtonId}`).addClass('d-none');
    });
}