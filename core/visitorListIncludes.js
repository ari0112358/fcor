module.exports = {
    flexItems: [
        './partials/visitor/flexItems/visitor_flex_1',
        './partials/visitor/flexItems/visitor_flex_2',
        './partials/visitor/flexItems/visitor_flex_3',
    ],
    additionalCSS: [
        'https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap4.min.css',
        'https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.css" rel="stylesheet" type="text/css'
    ],
    additionalJS: [
        'https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js',
        'https://cdn.datatables.net/1.10.21/js/dataTables.bootstrap4.min.js',
        'https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js',
        './js/docket/moment.js',
        './js/visitorList/visitorList_data.js',
        './js/visitorList/visitorList.js'
    ]
}