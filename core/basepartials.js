module.exports = {
    head: './partials/htmlbase/head',
    header: './partials/htmlbase/header',
    footer: './partials/htmlbase/footer',
    sidebar: './partials/htmlbase/sidebar',
    jsincludes: './partials/jsincludes/base'
}