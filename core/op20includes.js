module.exports = {
    flexItems: ['./partials/op20_flexitems/op20_flex_1'],
    additionalCSS: ['https://cdn.datatables.net/1.11.1/css/dataTables.bootstrap5.min.css'],
    additionalJS: [
        'https://cdn.datatables.net/1.11.1/js/jquery.dataTables.min.js',
        'https://cdn.datatables.net/1.11.1/js/dataTables.bootstrap5.min.js',
        './js/op20/op20.js'
    ]
}