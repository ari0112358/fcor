module.exports = {
    flexItems: [
        './partials/fileUpload/flexItems/upload_flex_1',
    ],
    additionalJS: [
        './js/fileUpload/fileUpload.js',
    ],
    additionalCSS: [
        './css/fileUpload.css'
    ]
}