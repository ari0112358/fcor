module.exports = {
    flexItems: [
        './partials/inmate/flexItems/inmate_flex_1',
        './partials/inmate/flexItems/inmate_flex_2',
    ],
    modalItems: [
        './partials/inmate/modals/add_inmate', 
    ],
    modalNames: ['Add Inmate'],
    additionalCSS: [
        'https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap4.min.css',
        'https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.css"'
    ],
    additionalJS: [
        'https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js',
        'https://cdn.datatables.net/1.10.21/js/dataTables.bootstrap4.min.js',
        'https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js',
        './js/inmateList/inmateList_data.js',
        './js/inmateList/inmateList.js',
    ]
}