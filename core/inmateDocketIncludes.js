module.exports = {
    flexItems: [
        './partials/inmateDocket/flexItems/docket_flex_1',
        './partials/inmateDocket/flexItems/docket_flex_2',
        './partials/inmateDocket/flexItems/docket_flex_3',
        './partials/inmateDocket/flexItems/docket_flex_4',
    ],
    additionalCSS: [
        'https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap4.min.css',
        'https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.css" rel="stylesheet" type="text/css',
        './css/calendar.min.css'
    ],
    additionalJS: [
        'https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js',
        'https://cdn.datatables.net/1.10.21/js/dataTables.bootstrap4.min.js',
        'https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js',
        './js/docket/moment.js',
        './js/inmateDocket/inmateDocket.js'
    ]
}