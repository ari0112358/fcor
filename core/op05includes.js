module.exports = {
    flexItems: ['./partials/op05_flexitems/op05_flex_1'],
    additionalCSS: ['https://cdn.datatables.net/1.11.1/css/dataTables.bootstrap5.min.css'],
    additionalJS: [
        'https://cdn.datatables.net/1.11.1/js/jquery.dataTables.min.js',
        'https://cdn.datatables.net/1.11.1/js/dataTables.bootstrap5.min.js',
        './js/op05/op05_data.js',
        './js/op05/op05.js'
    ]
}