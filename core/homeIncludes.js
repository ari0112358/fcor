module.exports = {
    flexItems: ['./partials/home_flexitems/flex_1'],
    modalItems: ['./partials/home_modals/modal_1', './partials/home_modals/modal_2'],
    modalNames: ['modal 1', 'modal 2'],
    additionalCSS: ['https://cdn.datatables.net/1.11.1/css/dataTables.bootstrap5.min.css'],
    additionalJS: [
        'https://cdn.datatables.net/1.11.1/js/jquery.dataTables.min.js',
        'https://cdn.datatables.net/1.11.1/js/dataTables.bootstrap5.min.js',
        './js/payeeSearch/payeeSearch_data.js',
        './js/payeeSearch/payeeSearch.js'
    ]
}