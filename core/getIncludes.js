const fs = require('fs');
const path = require('path');
const PARTIALS_PATH = './views/partials';
const FLEX_FOLDER = 'flexItems';
const MODAL_FOLDER = 'modals';

module.exports = {
    async getIncludes(moduleName){
        let includes = {
            flexItems: [],
            modals: []
        };
        try{
            let moduleFolder = path.join(PARTIALS_PATH, moduleName);
            if(fs.existsSync(moduleFolder)){
                let flexFolder = path.join(moduleFolder, FLEX_FOLDER);
                if(fs.existsSync(flexFolder)){
                    let flexItems = fs.readdirSync(flexFolder);
                    includes.flexItems = flexItems.map((flexItem) => {
                        return path.join(PARTIALS_PATH, moduleName, flexItem);
                    });
                }
                let modalFolder = path.join(moduleFolder, MODAL_FOLDER);
                if(fs.existsSync(modalFolder)){
                    let modals = fs.readdirSync(modalFolder);
                    includes.modals = modals.map((modal) => {
                        return path.join(PARTIALS_PATH, moduleName, modal);
                    })
                }
            } else {
                console.log('Module folder does not exist');
            }
        } catch(error){
            console.log(error)
        }
        return includes;
    }
}