'use strict'
const basepartials = require('../../core/basepartials')
const homeIncludes = require('../../core/homeIncludes')
const op20includes = require('../../core/op20includes')
const op05includes = require('../../core/op05includes')
const inmateIncludes = require('../../core/inmateListIncludes')
const visitorIncludes = require('../../core/visitorListIncludes')
const fileUploadIncludes = require('../../core/fileUploadIncludes')
const docketIncludes = require('../../core/docketIncludes')
const docketDetailsIncludes = require('../../core/docketDetailsIncludes')
const inmateDocketIncludes = require('../../core/inmateDocketIncludes')
const serviceIncludes = require('../../core/serviceIncludes')
const vnlListIncludes = require('../../core/vnlListIncludes')
const editVisitorIncludes = require('../../core/editVisitorIncludes')
//const getIncludes = require('../../core/getIncludes')

module.exports = (app) => {
    app.get('/', async (req, res) => {
        res.redirect('/inmateList');
        /* res.render('home', {
            ...basepartials,
            ...homeIncludes
        }) */
    })

    app.get('/op20', async (req, res) => {
        res.render('op20', {
            ...basepartials,
            ...op20includes
        })
    })
    app.get('/op05', async (req, res) => {
        res.render('op05', {
            ...basepartials,
            ...op05includes
        })
    })
    app.get('/inmateList', async (req, res)=> {
        res.render('inmateList', {
            ...basepartials,
            ...inmateIncludes,
        })
    })

    app.get('/visitorList', async (req, res) => {
        res.render('visitorList', {
            ...basepartials,
            ...visitorIncludes
        })
    })

    app.get('/editVisitor', async (req, res) => {
        res.render('editVisitor', {
            ...basepartials,
            ...editVisitorIncludes
        })
    })

    app.get('/fileUpload', async (req, res) => {
        res.render('fileUpload', {
            ...basepartials,
            ...fileUploadIncludes
        })
    })

    app.get('/docket', async (req, res) => {
        res.render('docket', {
            ...basepartials,
            ...docketIncludes
        })
    })

    app.get('/docketDetails', async (req, res) => {
        res.render('docket', {
            ...basepartials,
            ...docketDetailsIncludes
        })
    })

    app.get('/inmateDocket', async (req, res) => {
        res.render('inmateDocket', {
            ...basepartials,
            ...inmateDocketIncludes
        })
    })

    app.get('/services', async (req, res) => {
        res.render('inmateDocket', {
            ...basepartials,
            ...serviceIncludes
        })
    })

    app.get('/vnlList', async (req, res) => {
        res.render('inmateDocket', {
            ...basepartials,
            ...vnlListIncludes
        })
    })

    app.get('/aboutus', async(req, res) => {
        res.render('aboutus')
    })
    
}